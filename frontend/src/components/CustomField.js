import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ErrorContainer from './ErrorContainer';

const FieldContainer = styled.div`
    width: 80%;
    margin: 10px 0;
`;

const InputContainer = styled.div`
    width: 100%;
    font-size: 16px;
    position:relative;

    label{
        font-size: 14px;
        position: absolute;
        top: -10px;
        left: 10px;
        background: white;
        padding: 0 5px;
        transition: all 0.2s ease-in;   
        color: #b9b9b9;
    }

    input {
        font-size: 16px;
        width:100%
        border: 1px #b9b9b9 solid;
        padding:10px
        border-radius: 5px;
        transition: all 0.2s ease-in;   

    }

    input:focus{
        border-color: rgba(142, 14, 0, 0.8);
    }
    input:focus+label{
        color: rgba(142, 14, 0, 0.8);
    }
`;

const CustomField = ( { input, label, placeholder, type, meta: {touched, error} }) => (
    <FieldContainer>
        <InputContainer>
            <input {...input} placeholder={placeholder} type={type} onChange={value => input.onChange(value)}/>
            <label htmlFor={input.name}>{label}</label>
            {touched && error && <ErrorContainer><i className="fas fa-exclamation-circle"/>{error}</ErrorContainer>}
        </InputContainer>
    </FieldContainer>
);

CustomField.propTypes = {
    input: PropTypes.object.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    meta: PropTypes.shape({
        touched: PropTypes.bool,
        error: PropTypes.string
    }).isRequired,
    type: PropTypes.string.isRequired
};

export default CustomField;