import styled from 'styled-components';

export default styled.div`
color: #FF0000;
font-size: 14px;
padding-top: 10px;
svg{
    margin: 0 5px;
    font-size: 16px;
}
`;
