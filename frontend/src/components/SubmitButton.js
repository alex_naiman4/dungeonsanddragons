import styled from 'styled-components';

export default styled.button`
    background: linear-gradient(rgba(0, 0, 0, 1), rgb(142, 14, 0));
    width: 80%;
    font-size: 20px;
    padding: 10px;
    color: white;
    text-transform: uppercase;
    margin: 10px;
    border-radius: 5px;
    font-weight: 700;
    border: 1px solid rgba(0,0,0,0);
    &:hover{
        color: rgb(142, 14, 0);
        border: 1px solid rgba(142, 14, 0, 0.8);
        background: white;
    }
`;