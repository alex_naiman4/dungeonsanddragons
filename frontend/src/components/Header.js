import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const HeaderContainer = styled.div`
    width: 100vw;
    background: black;
    padding: 15px;
    padding-top: 15px;
    padding-right: 15px;
    padding-bottom: 15px;
    padding-left: 15px;
    font-size: 16px;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    position:fixed;
    top:0;
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    z-index: 100
`;

const LogoutContainer = styled.div`
    color:white;
    display:flex;
    padding:5px;
    background: rgb(142, 14, 0);
    margin: 0 20px 0 10px;
    border-radius: 5px;
    border: 1px solid rgba(0,0,0,0);
    svg{
        font-size:20px;
        margin-left:10px;
    }
    &:hover{
        color: rgb(142, 14, 0);
        border: 1px solid rgba(142, 14, 0, 0.8);
        background: white;
    }
`;

const UserContainer = styled.div`
    margin: 0 20px;
    color: white;
    background: black;
`;

const Header = ({user, logout}) => (
    <HeaderContainer>
        <UserContainer>
            {user.name}
        </UserContainer>
        <LogoutContainer onClick={logout}>
            Log Out  
            <i className="fas fa-sign-out-alt"/>
        </LogoutContainer>
    </HeaderContainer>
);

Header.propTypes = {
    user: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired
};

export default Header;