import styled from 'styled-components';

const HeroImage = styled.div`
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    min-height: 100vh;
    background: linear-gradient(rgba(0, 0, 0, 1), rgb(142, 14, 0));
    background-image: linear-gradient(rgba(0, 0, 0, 0.9), rgba(142, 14, 0, 0.8)), url(http://i.imgur.com/xgDCsSl.jpg);      /* Set a specific height */
    height: 100vh;      /* Position and center the image to scale nicely on all screens */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
    
`;
const HeroContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
    background: white;
    padding: 20px;
    border-radius: 5px;
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    width: 40%;
    min-width: 300px;
    max-width: 500px;
`;

export default {HeroContainer, HeroImage};