import styled from 'styled-components';

export default styled.div`
    font-size: 14px;
    padding-top: 60px;
    a{
        color: rgb(142, 14, 0);
        text-decoration: underline;
        padding-left: 10px
    }
`;
