import React from 'react';
import styled from "styled-components";

const LoaderContainer = styled.div`
    position:absolute;
    width:100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    color: white;
    font-size: 100px;
    z-index: 300;
    top: 0
`;

export default () => (
    <LoaderContainer>
        <i className="fas fa-spinner fa-spin"/>
    </LoaderContainer>
);