/* eslint-disable import/no-named-as-default */
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createGlobalStyle } from 'styled-components';
import { PersistGate } from 'redux-persist/lib/integration/react';

import {store, persistor} from './store/index';
import App from './routes/App';
import reset from './css/reset';

const GlobalStyle = createGlobalStyle`${reset}`;

ReactDOM.render(
    <BrowserRouter>
        <Fragment>
            <Provider store={store}>
                <PersistGate  persistor={persistor} >
                    <App />
                </PersistGate> 
            </Provider>
            <GlobalStyle />
        </Fragment>
    </BrowserRouter>,
    document.getElementById('root')
);
