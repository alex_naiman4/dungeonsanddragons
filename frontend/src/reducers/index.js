import { reducer as form } from 'redux-form';
import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {reducer as UserReducer} from './User';
import {reducer as DndReducer}  from './DndClasses';

const rootReducer = (state = []) => state;
const authPersistConfig = {
    key: 'dnd',
    storage,
    blacklist: ['error']
} ;
export default {
    rootReducer,
    user: persistReducer(authPersistConfig, UserReducer),
    dndClasses: DndReducer,
    form
};
