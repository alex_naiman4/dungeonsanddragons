import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
    getDndClassesRequest: [],
    getDndClassesSuccess: ['data'],
    getDndClassesError: ['error'],
    getDndClassRequest: ['dndClassId'],
    getDndClassSuccess: ['data'],
    getDndClassError: ['error']
});

export const DndClassesTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
    fetching: false,
    list: [],
    item: null,
    error: null
});

export const reducer = createReducer(INITIAL_STATE,
    {
        GET_DND_CLASSES_REQUEST: state => state.merge({fetching: true}),
        GET_DND_CLASSES_SUCCESS: (state, {data}) => state.merge({fetching: false, list: data}),
        GET_DND_CLASSES_ERROR: (state, {error}) => state.merge({fetching: false, error}),
        GET_DND_CLASS_REQUEST: (state) => state.merge({fetching: true}),
        GET_DND_CLASS_SUCCESS: (state, {data}) => state.merge({fetching: false, item: data}),
        GET_DND_CLASS_ERROR: (state, {error}) => state.merge({fetching: false, error})
    }
);