import { createReducer, createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
    loginRequest: ['email', 'password'],
    loginSuccess: ['data'],
    loginFailure: ['error'],
    logout: null,

    registerRequest: ['email', 'password', 'name'],
    registerSuccess: ['data'],
    registerFailure: ['error']
});

export const UserTypes = Types;
export default Creators;

const INITIAL_STATE = {
    fetching: false,
    errorRegister: null,
    ID: null,
    errorLogin: null
};

export const reducer = createReducer(INITIAL_STATE,
    {
        LOGIN_REQUEST: state => ({...state, fetching:true}),
        LOGIN_SUCCESS: (state, {data}) => ({...state, fetching: false, ...data}),
        LOGIN_FAILURE: (state, {error}) => ({...state, fetching: false, errorLogin:error}),
        LOGOUT: () => INITIAL_STATE,

        REGISTER_REQUEST: state => ({...state, fetching: true}),
        REGISTER_SUCCESS: (state, {data}) => ({...state, fetching: false, ...data}),
        REGISTER_FAILURE: (state, {error}) => ({...state, fetching: false, errorRegister:error})
    }
);