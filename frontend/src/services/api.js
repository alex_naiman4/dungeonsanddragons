// a library to wrap and simplify api calls
import apisauce from 'apisauce';

// our "constructor"
// http://localhost:8080/ is the address of the spring server
const create = (baseURL = 'http://localhost:8081/') => {
    const api = apisauce.create({
    // base URL is read from the "constructor"
        baseURL,
        // here are some default headers
        headers: {
        },
        // 10 second timeout...
        timeout: 10240,
        // withCredentials: true
    });

    return {
        getDndClasses: () => api.get('/classes'),
        getDndClass: (id) => api.get(`/classes/${id}`),
        login: (email, password) => api.post(`/app_login?email=${email}&password=${password}`),
        register: (email,password,name) => api.post(`/app_register?email=${email}&password=${password}&name=${name}`)
    };
};

export default {
    create
};
