import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Field, reduxForm, Form } from 'redux-form';
import {Link} from 'react-router-dom';
import UserActions from '../reducers/User';
import CustomField from '../components/CustomField';

import Hero from '../components/HeroImage';
import ErrorContainer from '../components/ErrorContainer';
import SubmitButton from '../components/SubmitButton';
import RedirectContainer from '../components/RedirectContainer';

const Header =  styled.div`
    font-size: 30px;
    font-weight: 800;
    text-transform: uppercase;
    letter-spacing: 4px;
    padding: 20px;
    text-align: center
`;

const StyledForm = styled(Form)`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
`;

const validate = values => {
    const errors = {};
    if(!values.email){
        errors.email = 'Field Required';
    } else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)){
        errors.email = 'Invalid email address';
    }
    if(!values.name){
        errors.name = 'Field required';
    }

    if(!values.password){
        errors.password = 'Field Required';
    } else if(!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/i.test(values.password)){
        errors.password = 'Password must have minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character';
    }
    if(!values.repeatPassword){
        errors.repeatPassword = 'Field Required';
    } else if (values.repeatPassword!==values.password){
        errors.repeatPassword = 'Passwords do not match';
    }
    return errors;
};

class Register extends React.PureComponent {

    static defaultProps = {
        submitting: false
    };

    static propTypes = {
        register: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        submitting: PropTypes.bool
    };

    handleClickLogin = (values) => {
        const {register} = this.props;
        register(values.email, values.password, values.name);
    };
    
    render() {
        const { handleSubmit,submitting, user:{errorRegister} } = this.props;
        return (
            <Hero.HeroImage>
                <Hero.HeroContainer >
                    <Header>Create account</Header>
                    <StyledForm onSubmit={handleSubmit(this.handleClickLogin)}>
                        <Field
                            label="Email"
                            name="email"
                            type="text"
                            placeholder="johnDoe@email.com"
                            component={CustomField}
                        />
                        <Field
                            label="Name"
                            name="name"
                            type="text"
                            placeholder="John Doe"
                            component={CustomField}
                        />
                        <Field
                            label="Password"
                            name="password"
                            type="password"
                            placeholder="PleaseNotYourMomsBirthday"
                            component={CustomField}
                        />
                        <Field
                            label="Repeat password"
                            name="repeatPassword"
                            type="password"
                            placeholder="PleaseNotYourMomsBirthday"
                            component={CustomField}
                        />
                        <SubmitButton type="submit" disabled={submitting}>
                            Sign up
                        </SubmitButton>
                    </StyledForm>
                    {errorRegister && <ErrorContainer><i className="fas fa-exclamation-circle"/>{errorRegister}</ErrorContainer>}
                    <RedirectContainer>
                        Have already an account?
                        <Link to="/">Login here</Link>
                    </RedirectContainer>
                </Hero.HeroContainer>
            </Hero.HeroImage>);
    }
}

export default connect(
    state => ({
        user: state.user
    }),
    {
        register: UserActions.registerRequest
    }
)(reduxForm({ form: 'register', validate })(Register));