import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Field, reduxForm, Form } from 'redux-form';
import {Link} from 'react-router-dom';
import UserActions from '../reducers/User';
import CustomField from '../components/CustomField';

import Hero from '../components/HeroImage';
import ErrorContainer from '../components/ErrorContainer';
import SubmitButton from '../components/SubmitButton';
import RedirectContainer from '../components/RedirectContainer';

const Header =  styled.div`
    font-size: 30px;
    font-weight: 800;
    text-transform: uppercase;
    letter-spacing: 4px;
    padding: 20px;
    text-align: center

`;

const StyledForm = styled(Form)`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
`;

const validate = values => {
    const errors = {};
    if(!values.email){
        errors.email = 'Field Required';
    } else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)){
        errors.email = 'Invalid email address';
    }
    if(!values.password){
        errors.password = 'Field Required';
    }
    return errors;
};

class Login extends React.PureComponent {

    static defaultProps = {
        submitting: false
    };

    static propTypes = {
        login: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        submitting: PropTypes.bool
    };

    handleClickLogin = (values) => {
        const {login} = this.props;
        login(values.email, values.password);
    };
    
    render() {
        const { handleSubmit,submitting, user:{errorLogin} } = this.props;
        return (
            <Hero.HeroImage>
                <Hero.HeroContainer >
                    <Header>Log in</Header>
                    <StyledForm onSubmit={handleSubmit(this.handleClickLogin)}>
                        <Field
                            label="Email"
                            name="email"
                            type="text"
                            placeholder="johnDoe@email.com"
                            component={CustomField}
                        />
                        <Field
                            label="Password"
                            name="password"
                            type="password"
                            placeholder="PleaseNotYourMomsBirthday"
                            component={CustomField}
                        />
                        <SubmitButton type="submit" disabled={submitting}>
                            Sign in
                        </SubmitButton>
                    </StyledForm>
                    {errorLogin && <ErrorContainer><i className="fas fa-exclamation-circle"/>{errorLogin}</ErrorContainer>}
                    <RedirectContainer>
                        Don&apos;t you have an account?
                        <Link to="/register">Create one here</Link>
                    </RedirectContainer>
                </Hero.HeroContainer>
            </Hero.HeroImage>);
    }
}

export default connect(
    state => ({
        user: state.user
    }),
    {
        login: UserActions.loginRequest
    }
)(reduxForm({ form: 'login', validate })(Login));