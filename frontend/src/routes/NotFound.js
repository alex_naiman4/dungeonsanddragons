import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import {Container, Heading} from 'styled-minimal';
import GradientContainer from '../components/GradientContainer';

const StyledContainer = styled(Container)`
    align-items: center;
    text-align: center;
    h1,
    a {
        color: #fff;
        line-height: 1;
    }

    a {
        text-decoration: underline;
    }
`;

const NotFound = () => (
    <GradientContainer>
        <StyledContainer verticalPadding layout="fullScreen" >
            <Heading fontSize={100}>404</Heading>
            <Heading as="h2">Page Not Found</Heading>
            <Link to="/">
                <Heading as="h2">go home</Heading>
            </Link>
        </StyledContainer>
        
    </GradientContainer>

);
export default NotFound;    