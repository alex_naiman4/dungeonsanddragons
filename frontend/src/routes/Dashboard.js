import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styled, {css} from 'styled-components';
import DndClassesActions from '../reducers/DndClasses';
import LoaderContainer from '../components/Loader';

const DashboardContainer = styled.div`
    height: 100vh;
    display: flex
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    padding-top: 50px;
`;

const DndClassWrapper = styled.div`
    min-width: 300px;
    height: 350px; 
    width: 30%;
    padding: 20px;
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    &:hover{
        transform: scale(1.05)
    }
`;

const DndClassContainer = styled.div`
    background: #f7ffca;
    width: 100%;
    height: 100%;
    border-radius: 5px;
    box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    position: relative;
    &:hover{
        box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
    }
`;

const Avatar = styled.div`
    bottom: 0;
    right: 0;
    height: 100%;
    position: absolute;
    background-size: contain;
    width: 100%;
    object-fit: contain;
    background-repeat: no-repeat;
    background-position: right bottom;
    border-radius:5px;
    ${props => props.avatar && css`
        background-image: url(${props.avatar})`}
`;

const ClassInfoContainer = styled.div`
    padding: 5px;
    border: 1px solid #d2d2d2;
    background: rgba(255,255,255,0.8);
    margin: 20px;
    height: 50%;
    width: 70%;
    position: absolute;
    z-index: 1;
    bottom:0;
    p{
        font-size: 14px;
        span{
            font-weight: 800
            padding:5px;
        }
    }
`;

const DndClassName = styled.div`
    padding:20px;
    font-size:25px;
    font-weight: 800;
    position:relative;
    z-index: 1;
    display:flex;
    align-items:center;
    img{
            width:40px;
            height:40px;
            margin:5px;
            border-radius:5px;
    }
`;

const NoContent = styled.div`
    color: white;
    font-size: 40px;
    font-weight: 800;
`;

export class Dashboard extends React.PureComponent {
    static propTypes = {
        getClasses: PropTypes.func.isRequired,
        dndClasses: PropTypes.arrayOf(Object).isRequired,
        history: PropTypes.object.isRequired,
        fetching: PropTypes.bool.isRequired
    };

    componentDidMount () {
        const {getClasses} = this.props;
        getClasses();
    }

    render() {
        const {dndClasses, history, fetching} = this.props;
        return (
            <DashboardContainer>
                {fetching && <LoaderContainer />}
                {dndClasses.length !== 0 ? dndClasses.map((k) => (
                    <DndClassWrapper key={k.id}>
                        <DndClassContainer onClick={()=>history.push(`/dashboard/${k.id}`)}>
                            <Avatar avatar={k.avatar} />
                            <ClassInfoContainer>
                                <p>{k.description}</p>
                                <p><span>Hit Die:</span>{k.hitDie}</p>
                                <p><span>Saves:</span>{k.saves.replace('&amp;', '&')}</p>
                                <p><span>Primary Ability:</span>{k.primaryAbility.replace('&amp;', '&')}</p>
                            </ClassInfoContainer>
                            <DndClassName><img src={k.imgIcon} alt={k.name}/>{k.name}</DndClassName>
                        </DndClassContainer>
                    </DndClassWrapper>
                )):
                    (!fetching && <NoContent>No item available</NoContent>)}
            </DashboardContainer>);
    }
}

export default connect(
    state => ({
        users: state.user,
        dndClasses: state.dndClasses.list,
        fetching: state.dndClasses.fetching,
        error: state.dndClasses.error
    }),
    {
        getClasses: DndClassesActions.getDndClassesRequest,
    }
)(Dashboard);