import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import styled, {css} from 'styled-components';
import DndClassesActions from '../reducers/DndClasses';
import Loader from '../components/Loader';

const DescriptionContainer = styled.div`
    padding: 40px;
    font-size: 16px;
    color: white
`;

const DndClassName = styled.div`
    padding:20px;
    font-size:25px;
    font-weight: 800;
    position:relative;
    z-index: 1;
    display:flex;
    align-items:center;
    img{
            width:40px;
            height:40px;
            margin:5px;
            border-radius:5px;
    }
`;

const Avatar = styled.div`
    height: 400px;
    background-size: contain;
    width: 300px;
    object-fit: contain;
    background-repeat: no-repeat;
    border-radius:5px;
    ${props => props.avatar && css`
        background-image: url(${props.avatar})`}
`;

const HeaderContainer = styled.div`
    padding-top: 70px;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    color: white;
    font-size: 20px;
    flex-wrap: wrap;
`;

const InfoContainer = styled.div`
    p{
        padding-left:25px;
        font-size: 16px;
        span{
            font-weight:800;
        }
    }
`;

const ErrorContainer = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 40px;
    font-weight: 800;
    color: white;
    flex-direction: column;
    a{
        text-decoration: underline;
    }
`;

export class DndClassItem extends React.PureComponent {
    static defaultProps = {
        dndClass: null,
        error: ""
    }

    static propTypes = {
        match: PropTypes.object.isRequired,
        getClass: PropTypes.func.isRequired,
        dndClass: PropTypes.object,
        history: PropTypes.object.isRequired,
        fetching: PropTypes.bool.isRequired,
        error: PropTypes.string
    };

    componentDidMount () {
        const {getClass, match: {params: {dndClassId}}, history } = this.props;
        if(Number.isNaN(Number(dndClassId)))
            history.push("/dashboard");
        getClass(dndClassId);
    }

    render() {
        const { dndClass, fetching, error } = this.props;
        return (
            <div>
                {dndClass && 
                    <div>
                        <HeaderContainer>
                            
                            <Avatar avatar={dndClass.fullImg} />
                            <InfoContainer>
                                <DndClassName><img src={dndClass.imgIcon} alt={dndClass.name}/>{dndClass.name}</DndClassName>
                                <p><span>Hit Die:</span>{dndClass.hitDie}</p>
                                <p><span>Saves:</span>{dndClass.saves.replace('&amp;', '&')}</p>
                                <p><span>Primary Ability:</span>{dndClass.primaryAbility}</p>
                            </InfoContainer>
                        </HeaderContainer>
                        <DescriptionContainer>{dndClass.fullDescription.substr(0,2000)}</DescriptionContainer>
                    </div>
                }
                {fetching && <Loader/>}
                {error && <ErrorContainer>
                    {error}
                    <Link to="/">
                        go home
                    </Link>
                </ErrorContainer>}
            </div>
        );
    }
}

export default connect(
    state => ({
        users: state.user,
        dndClass: state.dndClasses.item,
        fetching: state.dndClasses.fetching,
        error: state.dndClasses.error
    }),
    {
        getClass: DndClassesActions.getDndClassRequest,
    }
)(DndClassItem);