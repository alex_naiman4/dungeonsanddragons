import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Router, Switch, Route } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';
import UserActions from '../reducers/User';

import NotFound from './NotFound';
import Login from './Login';
import Register from './Register';
import Dashboard from './Dashboard';
import DndClassItem from './DndClassItem';

import GlobalStyles from '../components/GlobalStyles';
import RoutePublic from '../components/RoutePublic';
import RoutePrivate from '../components/RoutePrivate';

import theme from '../modules/theme';
import history from '../modules/history';
import Header from '../components/Header';

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  opacity: 1 !important;
  position: relative;
  transition: opacity 0.5s;
`;

const Main = styled.main`
  min-height: 100vh;
`;

const App = ({user,logout}) => (
    <Router history={history}>
        <ThemeProvider theme={theme}>
            <AppWrapper logged={!!user.ID}>
                {!!user.ID && <Header user={user} logout={logout}/>}
                <Main isAuthenticated={false}>
                    <Switch>
                        <RoutePublic
                            isAuthenticated={!!user.ID}
                            path="/"
                            to="dashboard"
                            exact
                            component={Login}
                        />
                        <RoutePublic
                            isAuthenticated={!!user.ID}
                            path="/register"
                            to="dashboard"
                            exact
                            component={Register}
                        />
                        <RoutePrivate
                            isAuthenticated={!!user.ID}
                            path="/dashboard"
                            exact
                            component={Dashboard}
                        />
                        <RoutePrivate
                            isAuthenticated={!!user.ID}
                            path="/dashboard/:dndClassId"
                            component={DndClassItem}
                        />
                        <Route component={NotFound} />
                    </Switch>
                </Main>
                <GlobalStyles />
            </AppWrapper>
        </ThemeProvider>
    </Router>
);
App.propTypes = {
    user: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired
};
export default connect(
    state => ({
        user: state.user
    }),
    {
        logout: UserActions.logout
    }
)(App);