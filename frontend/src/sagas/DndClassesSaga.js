import { put, call } from 'redux-saga/effects';
import DndClassesActions from '../reducers/DndClasses';

export function * getDndClasses (api) {
    const response = yield call(api.getDndClasses);
    if (response.status === 200) {
        yield put(DndClassesActions.getDndClassesSuccess(response.data));
    } else {
        yield put(DndClassesActions.getDndClassesError('error'));
    }
}

export function * getDndClass (api,{dndClassId}) {
    const response = yield call(api.getDndClass, dndClassId);
    if (response.status === 200) {
        yield put(DndClassesActions.getDndClassSuccess(response.data));
    } else {
        yield put(DndClassesActions.getDndClassError('Cannot load page'));
    }
}