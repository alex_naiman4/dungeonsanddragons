import { put, call } from 'redux-saga/effects';
import UserActions from '../reducers/User';

export function * login (api, {email, password}) {
    const response = yield call(api.login, email, password);
    if (response.status === 200) {
        yield put(UserActions.loginSuccess(response.data));
    } else {
        yield put(UserActions.loginFailure('Email or password is incorrect!'));
    }
}

export function * register (api,{email, password, name}) {
    const response = yield call(api.register, email, password, name);
    if (response.status === 200) {
        yield put(UserActions.registerSuccess(response.data));
    } else {
        yield put(UserActions.registerFailure('Email taken'));
    }
}

