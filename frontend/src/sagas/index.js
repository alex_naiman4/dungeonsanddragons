import {all, takeLatest} from 'redux-saga/effects';
import {getDndClasses, getDndClass} from './DndClassesSaga';
import {DndClassesTypes} from '../reducers/DndClasses';

import {UserTypes} from '../reducers/User';
import {login, register} from './UserSaga';

import API from '../services/api';

const api = API.create();

export default function* root() {
    yield all([
        takeLatest(DndClassesTypes.GET_DND_CLASSES_REQUEST, getDndClasses, api),
        takeLatest(DndClassesTypes.GET_DND_CLASS_REQUEST, getDndClass, api),
        takeLatest(UserTypes.LOGIN_REQUEST, login, api),
        takeLatest(UserTypes.REGISTER_REQUEST, register, api)
    ]);
}