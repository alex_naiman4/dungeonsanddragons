# Small web scrapper I made for getting all the data I wanted because I could not find an API for this theme I chosen
# It seemed fun to made

from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import json


class MyEncoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


class DndClass():
    id = 0

    def __init__(self, name, imgUrl, description, hitDie, primaryAbility, saves, imgIcon, fullDescription, fullImg):
        self.fullImg = fullImg
        self.fullDescription = fullDescription
        self.imgIcon = imgIcon
        self.name = name
        self.imgUrl = imgUrl
        self.description = description,
        self.hitDie = hitDie
        self.primaryAbility = primaryAbility
        self.saves = saves
        self.ID = DndClass.id
        DndClass.id+=1

    def toJson(self):
        return json.dumps(self.__dict__)

    def __repr__(self):
        return self.toJson()


def log_error(e):
    """
    It is always a good idea to log errors. 
    This function just prints them, but you can
    make it do anything.
    """
    print(e)

raw_html = simple_get('https://www.dndbeyond.com/characters/classes')
html = BeautifulSoup(raw_html, 'html.parser')
divClasses = html.findAll("div", {"class": "listing-card__content"})
divClasses.pop()
jsonData = []
for i in divClasses:
    imgBackground = str(i.find("div", {"class": "listing-card__bg"})["style"]).split("url('")[1].replace("');", '')
    imgIcon = i.find("img", {"class": "listing-card__icon"})["src"]
    name = i.find("h3", {"class": "listing-card__title"}).text.strip()
    shortDescription = i.find("p").text
    hitDie = str(i.find("p", {"class": "characters-statblock"})).split("<br>")[0].replace(
        '<p class="characters-statblock" style="font-family:Roboto Condensed;font-size:14px"><strong>Hit Die:</strong>',
        '')
    primaryAbility = str(i.find("p", {"class": "characters-statblock"})).split("<br>")[1].replace(
        '<strong>Primary Ability:</strong>', '')
    saves = str(i.find("p", {"class": "characters-statblock"})).split("<br>")[2].replace('<strong>Saves:</strong>',
                                                                                         '').replace('</br></br></p>',
                                                                                                     '')
    class_html = simple_get(str('https://www.dndbeyond.com/characters/classes/' + name))
    class_divs = BeautifulSoup(class_html, 'html.parser')
    fullImgSrc = class_divs.find("img", {"class": "image"})["src"]
    invalid_tags = ['b', 'i', 'u', 'br', 'a', 'img', 'script', 'span']
    container = class_divs.find("div", {"class": "content-container"})
    for match in container('script'):
        match.extract()
    for match in container('em'):
        match.extract()
    for match in container('strong'):
        match.extract()
    for match in container.findAll(invalid_tags):
        match.unwrap()
    data = ''.join(p.get_text(strip=True) for p in container.findAll('p'))
    # print(class_divs.find("div",{"class":"content-container"}).findAll('p').map(lambda k : k.text))
    jsonData.append(
        DndClass(name, imgBackground, shortDescription, hitDie, primaryAbility, saves, imgIcon, data, fullImgSrc))
with open("data_file.json", "w") as write_file:
    json.dump(jsonData, write_file, default=lambda x: x.__dict__)

# print((str(i.find("div", {"class": "listing-card__bg"})["style"]).split("url('"))[1].replace("');",'')) #url backgrounf
# print(i.find("img", {"class": "listing-card__icon"})["src"]) #url icon
# print(i.find("h3", {"class": "listing-card__title"}).text) # name
# print(i.find("p").text) # description
# print(str(i.find("p", {"class": "characters-statblock"})).split("<br>")[0].replace('<p class="characters-statblock" style="font-family:Roboto Condensed;font-size:14px"><strong>Hit Die:</strong>','')) # hitDie
# print(str(i.find("p", {"class": "characters-statblock"})).split("<br>")[1].replace('<strong>Primary Ability:</strong>','')) # primary ability
# print(str(i.find("p", {"class": "characters-statblock"})).split("<br>")[2].replace('<strong>Saves:</strong>','').replace('</br></br></p>','')) # name