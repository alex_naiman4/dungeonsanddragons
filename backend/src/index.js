// import dependecies
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet')
const morgan = require('morgan')


//  define the express server app

const app = express();
function generateFunc(){
	var inc = 1;
	function aux(){
		inc = inc + 1;
		return inc;
	}
	return aux;
}

const generate = generateFunc()

// run-time database

const classes = require('./data_file.json');
const users = [
	{
		ID:1,
		email:'johnDoe@email.com',
		password: 'password',
		name: 'John Doe'
	}
]
// enhance app security with Helmet
app.use(helmet());

// used bodyParser to parse application/json content-type
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// enable all CORS requests
app.use(cors());

// log HTTP requests
app.use(morgan('combined'));

// retrieve all dnd classes
app.get('/classes', (req, res) => {
    const shortClass = classes.map(cl => ({
		id: cl.ID,
		name: cl.name,
		description: cl.description[0],
		hitDie: cl.hitDie,
		saves: cl.saves,
		imgIcon: cl.imgIcon,
		primaryAbility: cl.primaryAbility,
		avatar: cl.imgUrl,
    }));
    res.send(shortClass);
  });
  

// get a specific class
app.get('/classes/:id', (req, res) => {
    const cls = classes.filter(cl => (cl.ID === parseInt(req.params.id)));
    if (cls.length > 1) return res.status(500).send();
    if (cls.length === 0) return res.status(404).send();
    res.send(cls[0]);
});

app.post('/app_login', (req, res) => {
    let email = req.param('email');
    let password = req.param('password');
    let items = users.filter(i=> i.email === email && i.password === password);
    if (items.length !== 1){
        res.status(401).send();
		return;
	}
    res.send({
		email: items[0].email,
		name: items[0].name,
		ID: items[0].ID
	});
})

app.post('/app_register', (req, res) => {
	var email = req.query.email;
    var password = req.query.password;
	var name = req.query.name;
	var items = users.filter(i=> i.email === email);
    if (items.length !== 0){
			res.status(409).send();
			return;
	}
	let newId = generate()
	const newUser = {
			name,
			password,
			email,
			ID: newId
	}
	users.push(newUser)
	res.send({
		name:name,
		email:email,
		ID: newId 
	})
	
})

// start the server
app.listen(8081, () => {
    console.log('listening on port 8081');
});