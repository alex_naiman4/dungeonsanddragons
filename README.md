
## Getting started

1. Clone this repo using `https://bitbucket.org/alex_naiman4/dungeonsanddragons.git`
2. Move to the appropriate directory: `cd dungeonsanddragons`.<br />
3. To install the frontend `cd frontend` then `npm install` to install dependencies.<br />
4. To install the frontend `cd backend` then `npm install` to install dependencies.

## Running
* For Backend
  - `cd backend`
  - `npm start`
  It should run at localhost:8081
* For Frontend
  - `cd frontend`
  - `npm start`
It should run at localhost:8080
Here you should be able to login with 
 - email: johnDoe@email.com
 - password: password